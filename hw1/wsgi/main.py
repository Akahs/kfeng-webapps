import cherrypy
import operator, os, pickle, sys
import mysql.connector
from collections import OrderedDict
from genshi.template import TemplateLoader

loader=TemplateLoader(
  os.path.join(os.path.dirname(__file__), 'templates'),
  auto_reload=True
)
diners={}
diners['Subway']=427
diners['Starbucks']=1230
diners['Burger King']=1836
diners['Club 23']=1481
diners=OrderedDict(sorted(diners.items(), key=lambda t: t[1]))

class ExampleApp(object):
    @cherrypy.expose
    def index(self):
        tmpl=loader.load('index.html')
        stream=tmpl.generate(restaurants=diners)
        return stream.render('html',doctype='html')
    @cherrypy.expose
    def showdb(self):
        cnx = mysql.connector.connect(user='test', password='mypass',
                              host='127.0.0.1',
                              database='testdb')
        cursor = cnx.cursor()
        query = ("SELECT firstname,lastname,email FROM Invitations")
        cursor.execute(query)
        info = str()
        print cursor
        for (firstname, lastname, email) in cursor:
           info = info + "Full Name:" + lastname + firstname + "Email: "+email
        return info
application = cherrypy.Application(ExampleApp(), None)
