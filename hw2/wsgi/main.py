import cherrypy
import operator, os, pickle, sys
import mysql.connector
from collections import OrderedDict
from genshi.template import TemplateLoader

#Define database variables
DATABASE_USER='root'
DATABASE_HOST='127.0.0.1'
DATABASE_NAME='HungryND'

#Create connection to MySQL
cnx=mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
cursor=cnx.cursor()

#Configure template path
loader=TemplateLoader(
  os.path.join(os.path.dirname(__file__), 'templates'),
  auto_reload=True
)

diners={}
diners['Subway']=427
diners['Starbucks']=1230
diners['Burger King']=1836
diners['Club 23']=1481
diners=OrderedDict(sorted(diners.items(), key=lambda t: t[1]))

class ExampleApp(object):	
    @cherrypy.expose
    def index(self):
        tmpl=loader.load('index.html')
        stream=tmpl.generate(restaurants=diners)
        return stream.render('html',doctype='html')

    @cherrypy.expose
    def restaurants(self):
	getRest=('SELECT restId, name FROM restaurants')
	cursor.execute(getRest)
	rests=cursor.fetchall()
	tmpl=loader.load('restaurants.html')
	stream=tmpl.generate(restaurants=rests)
	return stream.render('html',doctype='html')
    
    @cherrypy.expose
    def a_rest(self,restId='0'):

	getRest=("SELECT * FROM restaurants WHERE restId="+"'"+restId+"'")
	cursor.execute(getRest)
	rest=cursor.fetchone()
        add=rest[2]+rest[3]+', '+rest[4]+', '+rest[5]

        getMenu=("SELECT * FROM menus WHERE restId="+"'"+restId+"'")
        cursor.execute(getMenu)
        all_menu=cursor.fetchall()
        
	tmpl=loader.load('a_rest.html')
	stream=tmpl.generate(rest_name=rest[1], phone=rest[6], address=add, lat=rest[7], lng=rest[8], url=rest[9], menus=all_menu) 
	return stream.render('html', doctype='html')

    @cherrypy.expose
    def a_menu(self,menuId=0):

	# find name of the given menu
	getMenu=("SELECT menu_name,restId from menus WHERE menuId="+menuId)
	cursor.execute(getMenu)
	menu=cursor.fetchone()
	menuName=menu[0]
	restId=menu[1]

	# find the name of the restaurant
	getRest_name=("SELECT name FROM restaurants WHERE restId="+"'"+restId+"'")
	cursor.execute(getRest_name)
	restName=cursor.fetchone() 

	#Make a dictionary for this menu
	menuDict={}
	getSections=("SELECT * FROM sections WHERE menuId="+menuId)
	cursor.execute(getSections)
	sections=cursor.fetchall()
	for section in sections:
	    getItems=("SELECT * FROM items WHERE secId="+str(section[0]))
	    cursor.execute(getItems)
	    items=cursor.fetchall()
	    menuDict[section[0]]=items

	#render the webpage
	tmpl=loader.load('a_menu.html')
	stream=tmpl.generate(menu_name=menuName, rest_name=restName, secs=sections, this_menu=menuDict)
	return stream.render('html', doctype='html')

    @cherrypy.expose
    def showdb(self):
        cnx = mysql.connector.connect(user='test', password='mypass',
                              host='127.0.0.1',
                              database='testdb')
        cursor = cnx.cursor()
        query = ("SELECT firstname,lastname,email FROM Invitations")
        cursor.execute(query)
        info = str()
        print cursor
        for (firstname, lastname, email) in cursor:
           info = info + "Full Name:" + lastname + firstname + "Email: "+email
        return info
application = cherrypy.Application(ExampleApp(), None)
