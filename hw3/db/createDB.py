#Use this script to create the database tables
#This script does NOT populate the tables with any data

import mysql.connector

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'HungryND'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, use_unicode=True)
cursor = cnx.cursor()

###################################
## Create DB if it doesn't exist ##
###################################
dropDB=(("DROP DATABASE IF EXISTS %s") % (DATABASE_NAME))
cursor.execute(dropDB)
createDB = (("CREATE DATABASE IF NOT EXISTS %s DEFAULT CHARACTER SET utf8mb4") % (DATABASE_NAME))
cursor.execute(createDB)

###########################
## Switch to HungryND DB ##
###########################

useDB = (("USE %s") % (DATABASE_NAME))
cursor.execute(useDB)

###########################
## Drop all tables first ##
###########################

#Order_contents
dropTableQuery = ("DROP TABLE IF EXISTS order_contents")
cursor.execute(dropTableQuery)

#Orders
dropTableQuery = ("DROP TABLE IF EXISTS orders")
cursor.execute(dropTableQuery)

#Hours
dropTableQuery = ("DROP TABLE IF EXISTS hours")
cursor.execute(dropTableQuery)

#Items
dropTableQuery = ("DROP TABLE IF EXISTS items")
cursor.execute(dropTableQuery)

#Sections
dropTableQuery = ("DROP TABLE IF EXISTS sections")
cursor.execute(dropTableQuery)

#Menus
dropTableQuery=("DROP TABLE IF EXISTS menus")
cursor.execute(dropTableQuery)

#Restaurants
dropTableQuery = ("DROP TABLE IF EXISTS restaurants")
cursor.execute(dropTableQuery)

#Users
dropTableQuery = ("DROP TABLE IF EXISTS users")
cursor.execute(dropTableQuery)


########################
## Create tables next ##
########################


#Restaurants
createTableQuery = ('''CREATE TABLE restaurants (
						restId VARCHAR(20) NOT NULL,
						name VARCHAR(45) NOT NULL,
						address VARCHAR(100) NOT NULL,
						city VARCHAR(45) NOT NULL,
						state VARCHAR(20) NOT NULL,
						zip VARCHAR(10) NOT NULL,
						phone VARCHAR(20) NOT NULL,
						lat DECIMAL(10,8) NOT NULL,
						lng DECIMAL(11,8) NOT NULL,
                        url VARCHAR(100) NOT NULL,
                        country varchar(20) NOT NULL,
                        has_menu BOOLEAN NOT NULL,
                        resource_uri varchar(20) NOT NULL,
						PRIMARY KEY (restId))'''
                    )
cursor.execute(createTableQuery)

#Hours
createTableQuery = ('''CREATE TABLE hours (
						restId VARCHAR(20) NOT NULL,
                                                day enum ('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday') not null,
                                                open TIME NOT NULL,
                                                close TIME NOT NULL,
                                                PRIMARY KEY(restId,day,open),
                                                FOREIGN KEY(restId)
                                                REFERENCES restaurants(restId)
                                                ON DELETE CASCADE
                                          );'''
                    )
cursor.execute(createTableQuery)

#Menus
createTableQuery=('''CREATE TABLE menus (
						menuId INT NOT NULL AUTO_INCREMENT,
						menu_name VARCHAR(30) NOT NULL,
						restId VARCHAR(20) NOT NULL,
						PRIMARY KEY(menuId),
						FOREIGN KEY(restId)
						REFERENCES restaurants(restId)
						ON DELETE CASCADE
						);'''
						)
cursor.execute(createTableQuery)

#Sections
createTableQuery=('''CREATE TABLE sections (
						secId INT NOT NULL AUTO_INCREMENT,
						section_name VARCHAR(50) NOT NULL,
						menuId INT NOT NULL,
						PRIMARY KEY(secId),
						FOREIGN KEY(menuId)
						REFERENCES menus(menuId)
						ON DELETE CASCADE
						);'''
						)
cursor.execute(createTableQuery)

#Items
createTableQuery=('''CREATE TABLE items (
						itemId INT NOT NULL AUTO_INCREMENT,
						item_name VARCHAR(50) NOT NULL,
						price VARCHAR(10) NOT NULL,
						description VARCHAR(100) NOT NULL,
						secId INT NOT NULL,
						PRIMARY KEY(itemId),
						FOREIGN KEY(secId)
						REFERENCES sections(secId)
						ON DELETE CASCADE
						);'''
						)
cursor.execute(createTableQuery)

#Users
createTableQuery=('''CREATE TABLE users (
					userId VARCHAR(20) NOT NULL,
					phone VARCHAR(20) NOT NULL,
					address VARCHAR(50) NOT NULL,
					email VARCHAR(50) NOT NULL,
					PRIMARY KEY(userId)
					);'''
					)
cursor.execute(createTableQuery)

#Orders
createTableQuery=('''CREATE TABLE orders (
					orderId INT NOT NULL AUTO_INCREMENT,
					userId VARCHAR(20) NOT NULL,
					total VARCHAR(20) NOT NULL,
					status VARCHAR(20) NOT NULL,
					
					PRIMARY KEY(orderId),
					FOREIGN KEY(userId)
					REFERENCES users(userId)
					ON DELETE CASCADE
					);'''
					)
cursor.execute(createTableQuery)

#Order_contents
createTableQuery=('''CREATE TABLE order_contents (
					orderId INT NOT NULL,
					itemId INT NOT NULL,
					item_name VARCHAR(50) NOT NULL,
					item_price VARCHAR(10) NOT NULL,
					quantity INT NOT NULL,
					PRIMARY KEY(orderId, itemId),
					FOREIGN KEY(itemId)
					REFERENCES items(itemId)
					ON DELETE CASCADE,
					FOREIGN KEY(orderId)
					REFERENCES orders(orderId)
					ON DELETE CASCADE
					);'''
					)
cursor.execute(createTableQuery)
					

#Commit the data and close the connection to MySQL
cnx.commit()
cnx.close()
