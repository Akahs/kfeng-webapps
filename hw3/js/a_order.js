$(document).ready(function(){
	$('#placeButton').click(function(event){
		var vUrl = $(this).attr('href');
		$.ajax({
			type: "PUT",
			url: vUrl,
			dataType: 'json'
		}).done(function(){
			$('#placeButton').slideUp();
			alert("Your order has been placed!");
		})
	})
})
