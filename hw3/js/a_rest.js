$(document).ready(function(){

	//Hours
	$('#hours').click(function(){
		$('#hours').children('p').slideToggle('fast');
	});
	
	//Menus
	$('#menus').click(function(){
		$('#menu_list').slideToggle('fast');
	})
	
	//Sections
	$('.one_menu').click(function(event){
		
		var $sections=$('#section-sublist');
		/*
		if ($(this).hasClass('active')){
			$(this).removeClass('active');		
			$sections.slideUp();
			return;
		}
		*/
		$(this).addClass('active')
		$(this).siblings().not(this).removeClass('active')
		
		
		//Prevent the default link behavior
		/*
		event.preventDefault();
		*/
		
		//If the section list is shown, hide it
		/*if ($sections.is(':visible')) {
			$sections.slideUp();
			return;
		}*/
		
		//SlideUp all other sections
		//$('.section-sublist').not($sections).slideUp();
		
		//Get the section JSON data via Ajax
		$.ajax({
			type: 'GET',
			url: $(this).attr('id'),
			dataType: 'json'
		}).done(function(data){
			$sections.empty();
			var sections = data;
			for (var i=0, n = sections.length; i<n; i++) {
				var section = sections[i];
				$sections.append(
					$('<a>')
            .addClass('list-group-item no-gutter')
            .attr('href', section.href)
            .append(
            	$('<div>')
            		.text(section.name)
            		.addClass('list-group-item-heading no-gutter')
            )
				);
			}
		});
	});	
});
