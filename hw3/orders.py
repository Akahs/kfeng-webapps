import cherrypy
import operator, os, sys, os.path
sys.stdout = sys.stderr
import mysql.connector
from mysql.connector import Error
import logging
from genshi.template import TemplateLoader
import json
from config import conf
import apiutil
from apiutil import errorJSON, SESSION_KEY


#Define database variables
DATABASE_USER='root'
DATABASE_HOST='127.0.0.1'
DATABASE_NAME='HungryND'

#Create connection to MySQL
cnx=mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
cursor=cnx.cursor()

#Configure template path
loader=TemplateLoader(
  os.path.join(os.path.dirname(__file__), 'templates'),
  auto_reload=True
)

class Orders(object):
	exposed = True
	def __init__(self):
		self.a_order = A_order()
		self.a_item = A_item()
		
	def _cp_dispatch(self, vpath):
		if len(vpath) == 1:
			cherrypy.request.params['orderID'] = vpath.pop()
			return self.a_order
		if len(vpath) == 2:
			if vpath[0] == 'add_item':
				cherrypy.request.params['itemID'] = vpath.pop()
				vpath.pop()
				return self.a_item
			else:
				vpath.pop()
                        	cherrypy.request.params['orderID'] = vpath.pop()
                        	return self.a_order

		return vpath
		
	def GET(self):
		getOrders=('SELECT * FROM orders')
		cursor.execute(getOrders)
		orders_info=cursor.fetchall()
		tmpl=loader.load('orders.html')
		stream=tmpl.generate(orders=orders_info)
		return stream.render('html', doctype = 'html')

class A_order(object):
	exposed = True
	def GET(self, orderID):
		getOrder = ("SELECT * FROM orders where orderId =" + orderID)
		cursor.execute(getOrder)
		order_info = cursor.fetchall()
		getOrderContents = ('SELECT * FROM order_contents where orderId ='+ orderID)
		cursor.execute(getOrderContents)
		contents_info = cursor.fetchall()
		tmpl = loader.load('a_order.html')
		stream = tmpl.generate(contents = contents_info, orderId = orderID, order = order_info[0])
		return stream.render('html', doctype = 'html')

	def PUT(self, orderID):
		placeOrder = ("UPDATE orders SET status = 'placed' where orderId =" + orderID)
		cursor.execute(placeOrder)
		cnx.commit()
		result = {'orderID':orderID, 'status': 'placed'}
		return json.dumps(result)

class A_item(object):
	exposed = True
	def GET(self, itemID):
		q = "SELECT * from items where itemId =" + itemID
		cursor.execute(q)
		item_info = cursor.fetchall()
		if not item_info:
			return errorJSON(code = 9004, message = "Item doesn't exist")
		return json.dumps(item_info)
	
	@cherrypy.tools.json_in(force = False)
	def PUT(self, itemID):
		quantity = int(cherrypy.request.json["quantity"])
		#q = "select count(1) from orders where status = 'unplaced'"
		q = "SELECT * FROM orders WHERE status = 'unplaced'"
		cursor.execute(q)
		order_info = cursor.fetchall()
		#return json.dumps({})
		if not order_info:
			inputDict = {
				'userId': 'Akahs',
				'total':'0',
				'status': 'unplaced'
			}
			q = "INSERT INTO orders (userId, total, status) VALUES (%(userId)s, %(total)s, %(status)s);"
			cursor.execute(q, inputDict)
			cnx.commit()
		q = "SELECT * from orders where status = 'unplaced'"
		cursor.execute(q)
		order_info = cursor.fetchall()[0]
		#order_info = order_info[0]
		#return json.dumps({})
		q = "SELECT * from items where itemId =" + itemID
		cursor.execute(q)
		item_info = cursor.fetchall()[0]
		#return json.dumps({})
		inputDict = {
			'orderId': order_info[0],
			'itemId': itemID,
			'item_name': item_info[1],
			'item_price': item_info[2],
			'quantity': quantity
		}
		q = "INSERT INTO order_contents (orderId, itemId, item_name, item_price, quantity) VALUES (%(orderId)s, %(itemId)s, %(item_name)s,%(item_price)s, %(quantity)s) ON DUPLICATE KEY UPDATE quantity = quantity + %(quantity)s"
		cursor.execute(q,inputDict)
#		q = " insert into order_contents (orderId, itemId) values (10, 99)"
		cnx.commit()
#		except Error as e:
#			print "mysql error: %s" %e
#			return errorJSON(code=9002, message="Failed to add order item to shopping cart")
		#result = {'orderID':order_info[0], 'itemID':itemID, 'quantity':quantity, 'errors':[]}
		return json.dumps({'orderID':order_info[0], 'itemID': itemID, 'quantity': quantity})

application=cherrypy.Application(Orders(),None,conf)
