#Use this script to populate the |restaurant| table of the database

import mysql.connector
from mysql.connector import Error
import json
from decimal import *
import random
import logging
import sys

logging.basicConfig(filename='mysql.log', level=logging.DEBUG, 
                    format='%(asctime)s %(message)s')
                    
#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'HungryND'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME, charset='utf8mb4', use_unicode=True)
cursor = cnx.cursor()




#Load json file
inputFile = open('restaurantData.json','r')
restaurantDict = json.load(inputFile)
inputFile.close()

secId=0
#Loop through the restaurants and add info and menu to database
for key, restaurant in restaurantDict.iteritems():
	
	###############################
	## Add restaurant info first ##
	###############################

	inputDict = {
		'restId' : key,
		'name' : restaurant['name'],
		'address' : restaurant['street_address'],
		'city' : restaurant['locality'],
		'state' : restaurant['region'],
		'zip' : restaurant['postal_code'],
		'phone' : restaurant['phone'],
		'lat' : restaurant['lat'],
		'lng' : restaurant['long'],
		'url' : restaurant['website_url'],
		'country' : restaurant['country'],
		'has_menu' : restaurant['has_menu'],
		'resource_uri' : restaurant['resource_uri']
	}

	#Insert this info into the database
	addRestaurant = ("INSERT INTO restaurants (restId, name, address, city, state, zip, phone, lat, lng, url, country, has_menu, resource_uri) VALUES (%(restId)s,  %(name)s, %(address)s, %(city)s, %(state)s, %(zip)s, %(phone)s, %(lat)s, %(lng)s, %(url)s, %(country)s, %(has_menu)s, %(resource_uri)s)")
	cursor.execute(addRestaurant,inputDict)
	
	####################
	## Add hours info ##
	####################
	
	hourDict=restaurant['open_hours']
	for it, days in hourDict.iteritems():
		if days:
			for times in days:
				inputDict={
					'rest':key,
					'day':it,
					'open_time':times[0:8],
					'close_time':times[11:19]
				}
				addHour=("INSERT INTO hours (restId, day, open, close) VALUES (%(rest)s, %(day)s, %(open_time)s, %(close_time)s)")
				cursor.execute(addHour,inputDict)
				
	###################
	## Add menu info ##
	###################
	
	for menu in restaurant['menus']:
		inputDict = {
			'menu_name': menu['menu_name'],
			'restId': key
			}
		addMenu=("INSERT INTO menus (menu_name, restId) VALUES (%(menu_name)s, %(restId)s)")
		cursor.execute(addMenu, inputDict)
		
		######################
		## Add section info ##
		######################
		
		getId=("SELECT menuId FROM menus WHERE menu_name = %(menu_name)s AND restId = %(restId)s")
		cursor.execute(getId, inputDict)
		row=cursor.fetchone()
		menuId=row[0]
		#print(row);
		for sec in menu['sections']:
			inputDict = {
				'section_name' : sec['section_name'],
				'menuId' : menuId
				}
			addSec=("INSERT INTO sections (section_name, menuId) VALUES (%(section_name)s, %(menuId)s)")
			cursor.execute(addSec, inputDict)
			
			###################
			## Add item info ##
			###################	 
			
			secId+=1
			#getId=("SELECT secId FROM sections WHERE section_name = %(section_name)s AND menuId = %(menuId)s")
			#cursor.execute(getId, inputDict)  <-- kept on getting error here: illegal mix of collations latin... utf8...
			#row=cursor.fetchone()
			#secId=row[0]
			for sub in sec['subsections']:
				for content in sub['contents']:
					if content['type']=="SECTION_TEXT":
						continue
					if 'price' in content:
						price=content['price']
					else:
						price='Unknown'
					if 'description' in content:
						description=content['description']
					else:
						description='good'
					inputDict = {
						'item_name' : content['name'],
						'price' : price,
						'description' : description,
						'secId' : secId
						}
					addItem=("INSERT INTO items (item_name, price, description, secId) VALUES (%(item_name)s, %(price)s, %(description)s, %(secId)s)")
					cursor.execute(addItem,inputDict)

# Create a dummy user
'''inputDict = {
	'userId': 'Akahs',
	'phone': '111-111-1111',
	'address': 'Mars',
	'email': 'akahs.kj@live.cn'
	}
addUser=("INSERT INTO users (userId, phone, address, email) VALUES (%(userId)s, %(phone)s, %(address)s, %(email)s)")
cursor.execute(addUser,inputDict)

# Create a dummy order
inputDict = {
	'userId': 'Akahs',
	'total': 5.75,
	'status': 'placed'
}
addOrder = ("INSERT INTO orders (userId, total, status) VALUES (%(userId)s, %(total)s, %(status)s)")
cursor.execute(addOrder, inputDict)

inputDict = {
	'orderId': 1,
	'itemId': 100,
	'item_name': 'something with id = 100',
	'item_price': 5.75,
	'quantity': 1
}
addContents = ("INSERT INTO order_contents (orderId, itemId, item_name, item_price, quantity) VALUES (%(orderId)s, %(itemId)s, %(item_name)s, %(item_price)s, %(quantity)s)")
cursor.execute(addContents, inputDict)
'''
# Commit changes
cnx.commit()
cnx.close()

