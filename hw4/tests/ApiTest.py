import requests
import json
s = requests.Session()
host='http://52.0.66.197'   #  replace by your host here
s.headers.update({'Accept': 'application/json'})

r = s.get('http://52.0.66.197/api/restaurants',)
print r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())

r = s.post('http://52.0.66.197/api/restaurants')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.text)


r = s.put('http://52.0.66.197/api/orders/2')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())

r = s.get('http://52.0.66.197/api/orders/add_item/99')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())
    
r = s.put('http://52.0.66.197/api/orders/add_item/199', json=({"quantity":3}))
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())



